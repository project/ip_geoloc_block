<?php
/**
 * @file ip_geoloc_block.module
 * Will get your location from ip_geoloc and display a block if it matches
 */

/**
 * Implements hook_block_info().
 */
function ip_geoloc_block_block_info() {
  $blocks['ip_geoloc_block'] = array(
    'info' => t('Geolocated Block'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function ip_geoloc_block_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'ip_geoloc_block':
      $result = array();
      if ($_SESSION['ip_geoloc']['location'][variable_get('ip_geoloc_block_block_scope')] == variable_get('ip_geoloc_block_block_keyword')) {
        $result = array(
          '#markup' => variable_get('ip_geoloc_block_block_content'),
        );
      }
      if (variable_get('ip_geoloc_block_block_debug')) {
        $result['#markup'] .= "<h2>" . t('Debug Data') . "</h3>";
        $location = $_SESSION['ip_geoloc']['location'];
        foreach ($location as $key => $value) {
          $result['#markup'] .= "<dt>" . $key . "</dt><dd>" . $value . "</dd>";
        }
      }
      $block['subject'] = variable_get('ip_geoloc_block_block_subject');
      $block['content'] = $result;
      break;
  }
  return $block;
}

/**
 * Implements hook_block_configure().
 */
function ip_geoloc_block_block_configure($delta = '') {
  // This example comes from node.module.
  $form = array();
  if ($delta == 'ip_geoloc_block') {
    $form['ip_geoloc_block_block_content'] = array(
      '#type' => 'textarea',
      '#title' => t('Block Content'),
      '#default_value' => variable_get('ip_geoloc_block_block_content'),
    );
    $form['ip_geoloc_block_block_scope'] = array(
      '#type' => 'radios',
      '#title' => t('Location scope') . ":",
      '#options' => array(
        'locality' => 'Locality', 
        'administrative_area_level_1' => 'Administrative Area Level 1',
        'administrative_area_level_2' => 'Administrative Area Level 2',
        'route' => 'Route', 
        'country' => 'Country', 
        'country_code' => 'Country Code (2 letters)', 
        'postal_code' => 'Postal Code'
        ),
      '#default_value' => variable_get('ip_geoloc_block_block_scope'),
      '#description' => t('Use the debug mode setting to see what these values will be for your area'),
      '#required' => TRUE,
    );
    $form['ip_geoloc_block_block_keyword'] = array(
      '#type' => 'textfield',
      '#title' => t('Location name') . ":",
      '#default_value' => variable_get('ip_geoloc_block_block_keyword'),
      '#required' => TRUE,
    );
    $form['ip_geoloc_block_block_debug'] = array(
      '#type' => 'checkbox',
      '#title' => t('Debug mode'),
      '#description' => t('Will display current locale information if location is not matched'),
      '#default_value' => variable_get('ip_geoloc_block_block_debug'),
    );
  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function ip_geoloc_block_block_save($delta = '', $edit = array()) {
  // This example comes from node.module.
  if ($delta == 'ip_geoloc_block') {
    variable_set('ip_geoloc_block_block_content', $edit['ip_geoloc_block_block_content']);
    variable_set('ip_geoloc_block_block_scope', $edit['ip_geoloc_block_block_scope']);
    variable_set('ip_geoloc_block_block_keyword', $edit['ip_geoloc_block_block_keyword']);
    variable_set('ip_geoloc_block_block_debug', $edit['ip_geoloc_block_block_debug']);
  }
}
